﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugTrackerApp;
using System.Security.Cryptography;
using System.Text;
using System.Data.SqlServerCe;
using System.IO;
using System.Windows.Forms;



namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// check whether the populateListBox() in bughistory works
        /// </summary>
        [TestMethod]
        public void bughistory_populate_listboxtest()
        {
            //arrange
            string user = "user";
            string bug = "bug";
            BugHistory form = new BugHistory(user, bug);
            //act
            try
            {
                form.populateListBox();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// check whether checkInputs() in MyProjects works. this method is similar in all the other forms
        /// </summary>
        [TestMethod]
        public void checkinputtest()
        {
            //arrange
            string user = "user";
            MyProjects form = new MyProjects(user);

            //act
            try
            {        
                form.checkInputs();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// check whether populatelistbox() in myanswers works
        /// </summary>
        [TestMethod]
        public void myanswers_populatelistboxtest()
        {
            //arrange
            string user = "user";
            myAnswers form = new myAnswers(user);

            //act
            try
            {
                form.populateListBox();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// check whether populatelistbox() in MyProjects works
        /// </summary>
        [TestMethod]
        public void myprojects_populatelistboxtest()
        {
            //arrange
            string user = "user";
            MyProjects form = new MyProjects(user);

            //act
            try
            {
                form.populateListBox();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// checks whether cleartextboxes() in Register works.
        /// </summary>
        [TestMethod]
        public void register_cleartextbox_test()
        {
            //arrange
            Register form = new Register();

            //act
            try
            {
                form.cleartxtBoxes();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// checks whether combofill() in reportform works.
        /// </summary>
        [TestMethod]
        public void reportform_combofill_test()
        {
            //arrange
            string user = "user";
            ReportForm form = new ReportForm(user);

            //act
            try
            {
                form.combofill();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// check whether [rojectfill() in reportform works.
        /// </summary>
        [TestMethod]
        public void reportform_projectfill_test()
        {
            //arrange
            string user = "user";
            ReportForm form = new ReportForm(user);

            //act
            try
            {
                form.projectfill();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// check whether assignuserfill() in Reportform works.
        /// </summary>
        [TestMethod]
        public void reportform_asignuserfill_test()
        {
            //arrange
            string user = "user";
            ReportForm form = new ReportForm(user);

            //act
            try
            {
                form.asignuserfill();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// check whether cleartextboxes() in Reportform works.
        /// </summary>
        [TestMethod]
        public void reportform_cleartxtBoxes_test()
        {
            //arrange
            string user = "user";
            ReportForm form = new ReportForm(user);

            //act
            try
            {
                form.cleartxtBoxes();
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Checks whether MD5 hash works when encrypting passwords
        /// </summary>
        [TestMethod]
        public void md5test()
        {
            // arrange
            string initstring = "password";
            string expected = "5f4dcc3b5aa765d61d8327deb882cf99";
            //act
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(initstring));
            byte[] result = md5.Hash;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }
            strBuilder.ToString();
            //assert
            Assert.AreEqual(expected, strBuilder.ToString());
        }

        /// <summary>
        /// Checks whether string values are passed between forms
        /// </summary>
        [TestMethod]
        public void new_project_menutest()
        {
            //arrange
            string passedstring = "user";
            //act
            try
            {
                NewProject newForm = new NewProject(passedstring);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Checks whether string values are passed between forms
        /// </summary>
        [TestMethod]
        public void check_project_menutest()
        {
            //arrange
            string passedstring = "user";
            //act
            try
            {
                MyProjects newForm = new MyProjects(passedstring);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }

        }

        /// <summary>
        /// Checks whether string values are passed between forms
        /// </summary>
        [TestMethod]
        public void report_bug_menutest()
        {
            //arrange
            string passedstring = "user";
            //act
            try
            {
                ReportForm newForm = new ReportForm(passedstring);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Checks whether string values are passed between forms
        /// </summary>
        [TestMethod]
        public void fix_bug_menutest()
        {
            //arrange
            string passedstring = "user";
            //act
            try
            {
                BugList newForm = new BugList(passedstring);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Checks whether string values are passed between forms
        /// </summary>
        [TestMethod]
        public void my_query_menutest()
        {
            //arrange
            string passedstring = "user";
            //act           
            try
            {
                UserBugList newForm = new UserBugList(passedstring);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// Checks whether string values are passed between forms
        /// </summary>
        [TestMethod]
        public void my_answer_menutest()
        {
            //arrange
            string passedstring = "user";
            //act           
            try
            {
                myAnswers newForm = new myAnswers(passedstring);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }


        /// <summary>
        /// Checks whether connection is obtained
        /// </summary>

        [TestMethod]
        public void loginchecktest()
        {
            //arrange
            SqlCeConnection mySqlConnection;
            string connection = @"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf";
            //act          
            try
            {
                mySqlConnection = new SqlCeConnection(connection);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }



        /// <summary>
        /// Checks whether string values are passed between forms
        /// </summary>
        [TestMethod]
        public void bugsolutiontransfer()
        {
            //arrange
            string actualusername = "user";
            string actualbugname = "bug";
            //act          
            try
            {
                BugSolution newForm = new BugSolution(actualusername, actualbugname);
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// checks whether populateListBox() works in BugList
        /// </summary>
        [TestMethod]
        public void Buglistpopulatelistbox1()
        {
            string user = "user";
            BugList form = new BugList(user);

            try
            {
                form.populateListBox();
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        /// <summary>
        /// checks whether populateListBox2() works in BugList
        /// </summary>
        [TestMethod]
        public void Buglistpopulatelistbox2()
        {
            string user = "user";
            BugList form = new BugList(user);

            try
            {
                form.populateListBox2();
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

    }

}
