﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Security.Cryptography;

namespace BugTrackerApp
{
    public partial class HomePage : Form
    {
        public HomePage()
        {
            InitializeComponent();
            menuStrip1.Visible = false;
            logoutbutton.Visible = false;
            edit_user_button.Visible = false;
            textUserPassword.PasswordChar = '*';
        }

        /// <summary>
        /// initialises the connection
        /// </summary>
        SqlCeConnection mySqlConnection;
        public String authenticateduser;
        public string MyProperty { get; set; }


        /// <summary>
        /// passwords are hashed using this method to improve security of the system
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Md5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// allows user to log into the system and access internal data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Login_button(object sender, EventArgs e)
        {
            mySqlConnection =
                   new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");
            mySqlConnection.Open();
            if (checkcredentials())
            {
                string hashed_password = Md5Hash(textUserPassword.Text);
                String usercheck = "SELECT COUNT(*) FROM members WHERE username = '" + textUserName.Text + "' AND password = '" + hashed_password + "'";
                SqlCeCommand cmdcheckuser = new SqlCeCommand(usercheck, mySqlConnection);
                int userExist = (int)cmdcheckuser.ExecuteScalar();               

                if (userExist > 0)
                {
                    panel1.Visible = false;
                    menuStrip1.Visible = true;
                    registerbutton.Visible = false;
                    authenticateduser = textUserName.Text;
                    userName.Text = authenticateduser;
                    logoutbutton.Visible = true;
                    edit_user_button.Visible = true;
                }
                else
                {
                    MessageBox.Show("Error: User does not exist");
                }
            }
                                 

        }
        ///checks whether the credentials are correct
        public bool checkcredentials()
        {
            bool rtnvalue = true;

            if (
                string.IsNullOrEmpty(textUserName.Text)
                || string.IsNullOrEmpty(textUserPassword.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }
        /// <summary>
        /// opens the registration form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void register_Click(object sender, EventArgs e)
        {
            Register newForm = new Register();
            newForm.Show();
        }

        /// <summary>
        /// opens report bug form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Report_Bug_Click(object sender, EventArgs e)
        {       
            ReportForm newForm = new ReportForm(authenticateduser);         
            newForm.Show();
        }

        /// <summary>
        /// opens bug list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fix_Bug_Click(object sender, EventArgs e)
        {
            BugList newForm = new BugList(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// opens users bugs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void User_Bugs(object sender, EventArgs e)
        {
            UserBugList newForm = new UserBugList(authenticateduser);
            newForm.Show();

        }

        /// <summary>
        /// opens new project form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newProject(object sender, EventArgs e)
        {
            NewProject newForm = new NewProject(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// opens edit details form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void edit_user_details(object sender, EventArgs e)
        {
            Edit_Details newForm = new Edit_Details(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// opens my answers form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_answers(object sender, EventArgs e)
        {
            myAnswers newForm = new myAnswers(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// opens my projects form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MyProjects(object sender, EventArgs e)
        {
            MyProjects newForm = new MyProjects(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// shows the actions carried out when the logoout button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void logoutbutton_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            userName.Text = "Please login";
            textUserName.Text = textUserPassword.Text = "";
            edit_user_button.Visible = false;
            menuStrip1.Visible = false;
            logoutbutton.Visible = false;
            registerbutton.Visible = true;
        }

        /// <summary>
        /// opens new projects form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void new_project_menu(object sender, EventArgs e)
        {
            NewProject newForm = new NewProject(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// opens check projects form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void check_project_menu(object sender, EventArgs e)
        {
            MyProjects newForm = new MyProjects(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// opens report bug form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void report_bug_menu(object sender, EventArgs e)
        {
            ReportForm newForm = new ReportForm(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// opens fix bug form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fix_bug_menu(object sender, EventArgs e)
        {
            BugList newForm = new BugList(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// opens my query form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_query_menu(object sender, EventArgs e)
        {
            UserBugList newForm = new UserBugList(authenticateduser);
            newForm.Show();
        }

        /// <summary>
        /// opens my answers form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void my_answer_menu(object sender, EventArgs e)
        {
            myAnswers newForm = new myAnswers(authenticateduser);
            newForm.Show();
        }
    }
}
