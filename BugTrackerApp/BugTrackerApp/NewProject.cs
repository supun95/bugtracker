﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugTrackerApp
{
    public partial class NewProject : Form
    {
        /// <summary>
        /// users data being passsed between forms
        /// </summary>
        public string AuthUser { get; set; }
        /// <summary>
        /// connection intiated
        /// </summary>
        SqlCeConnection mySqlConnection;

        public NewProject(string user)
        {
            InitializeComponent();
            this.AuthUser = user;
            label1.Text = user;
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");
            mySqlConnection.Open();

        }
        
        /// <summary>
        /// clears text boxes once the action is done
        /// </summary>
        public void cleartxtBoxes()
        {
            
            textBox1.Text = textBox2.Text = "";
        }

        /// <summary>
        /// validates input values provided by the user
        /// </summary>
        /// <returns></returns>
        public bool checkInputs()
        {
            bool rtnvalue = true;
            String existingprojectcheck = "SELECT COUNT(*) FROM Projects WHERE projectname = '"+textBox1.Text+"'";
            SqlCeCommand cmdcheckuser = new SqlCeCommand(existingprojectcheck, mySqlConnection);
            int projectCount = (int)cmdcheckuser.ExecuteScalar();

            if (projectCount > 0)
            {
                MessageBox.Show("Error: Project name already exists");
                rtnvalue = false;

            }

            if (
                string.IsNullOrEmpty(textBox1.Text)
                || string.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }          
            return (rtnvalue);
        }

        /// <summary>
        /// inserts values provided by the user into the database
        /// </summary>
        /// <param name="projectname"></param>
        /// <param name="projectdesc"></param>
        /// <param name="date"></param>
        /// <param name="username"></param>
        /// <param name="commandString"></param>
        public void insertRecord(String projectname, String projectdesc, DateTime date, String username, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);
                cmdInsert.Parameters.AddWithValue("@projectname", projectname);
                cmdInsert.Parameters.AddWithValue("@projectdesc", projectdesc);
                cmdInsert.Parameters.AddWithValue("@date", date);
                cmdInsert.Parameters.AddWithValue("@username", username);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// checks for user validation and submits results
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submit_project(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                String commandString = "INSERT INTO Projects(projectname, projectdesc, date, username) VALUES (@projectname, @projectdesc, @date, @username)";

                insertRecord(textBox1.Text, textBox2.Text, DateTime.Now, this.AuthUser, commandString);
                MessageBox.Show("New Project "+ textBox1.Text+" created");
                cleartxtBoxes();
                this.Close();

            }
        }
    }
}
