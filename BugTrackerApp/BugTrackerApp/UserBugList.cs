﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugTrackerApp
{
    public partial class UserBugList : Form
    {
        /// <summary>
        /// users data being passed through between forms
        /// </summary>
        public string AuthUser { get; set; }
        public UserBugList(string user)
        {
            this.AuthUser = user;
            InitializeComponent();
            populateListBox();
            userlabel.Text = this.AuthUser;
        }

        /// <summary>
        /// connection to the database
        /// </summary>
        SqlCeConnection mySqlConnection;
        public String chosenbug;

        /// <summary>
        /// displays the bug list in a listbox
        /// </summary>
        public void populateListBox()
        {

            mySqlConnection =
                    new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");

            String selcmd = "SELECT bugname FROM Bugs WHERE username = '" + this.AuthUser + "'";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                listBoxnfbuglist.Items.Clear();

                while (mySqlDataReader.Read())
                {
                    listBoxnfbuglist.Items.Add(mySqlDataReader["bugname"]);
                }
                
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

     
        /// <summary>
        /// when a bug is selected from the listbox, it's details will be shown in textboxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxbuglist_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selcmd2 = "SELECT * FROM Bugs WHERE bugname = '" + listBoxnfbuglist.Text + "'";
            SqlCeCommand mySqlCommand2 = new SqlCeCommand(selcmd2, mySqlConnection);

            try
            {
                //textBox1.Text = "";
                SqlCeDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    checkbugbox.Text = mySqlDataReader2["bugname"].ToString();
                    textBox2.Text = mySqlDataReader2["status"].ToString();
                    textBox3.Text = mySqlDataReader2["datesubmitted"].ToString();
                    textBox4.Text = mySqlDataReader2["code"].ToString();
                    textBox5.Text = mySqlDataReader2["severity"].ToString();               
                    chosenbug = mySqlDataReader2["bugname"].ToString();
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        
        /// <summary>
        /// validatesuser input
        /// </summary>
        /// <returns></returns>
        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (
                string.IsNullOrEmpty(checkbugbox.Text))
            {
                MessageBox.Show("Error: Please select a bug from the listbox");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        /// <summary>
        /// opens the bugsolution form, user details and the chosen bug is passed through
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check_the_Bug(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                BugSolution newForm = new BugSolution(chosenbug, this.AuthUser);
                newForm.Show();
                this.Close();
            }           

        }

    }
}
