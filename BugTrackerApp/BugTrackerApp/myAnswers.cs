﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugTrackerApp
{
    public partial class myAnswers : Form
    {
        //users data which is being passed through forms
        public string AuthUser { get; set; }

        public myAnswers(string user)
        {
            InitializeComponent();
            this.AuthUser = user;
            label6.Text = this.AuthUser;
            populateListBox();
        }

        SqlCeConnection mySqlConnection;

        /// <summary>
        /// displays bugs answered by the particular user
        /// </summary>
        public void populateListBox()
        {

            mySqlConnection =
                    new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");

            String selcmd = "SELECT * FROM Bugs where fixersname = '"+this.AuthUser+"'";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);


            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                myanswerlist.Items.Clear();

                while (mySqlDataReader.Read())
                {                  
                    myanswerlist.Items.Add(mySqlDataReader["bugname"]);
                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// Displays details of the bugs answered by the particular user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void myanswerlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selcmd2 = "SELECT * FROM Bugs WHERE bugname = '" + myanswerlist.Text + "'";
            SqlCeCommand mySqlCommand2 = new SqlCeCommand(selcmd2, mySqlConnection);

            try
            {
                //textBox1.Text = "";
                SqlCeDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    textBox1.Text = mySqlDataReader2["status"].ToString();
                    textBox2.Text = mySqlDataReader2["datefixed"].ToString();
                    textBox3.Text = mySqlDataReader2["code"].ToString();                   
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// allows the user top open the bug history form.user details are passed through with the bugname
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void answer_bug_history(object sender, EventArgs e)
        {
            BugHistory newForm = new BugHistory(this.AuthUser, myanswerlist.Text);
            newForm.Show();
        }
    }
}
