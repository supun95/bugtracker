﻿namespace BugTrackerApp
{
    partial class BugList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxbuglist = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.userlabel = new System.Windows.Forms.Label();
            this.bugnamebox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.myassinedlist = new System.Windows.Forms.ListBox();
            this.checkhistory = new System.Windows.Forms.Button();
            this.severitybox = new System.Windows.Forms.ComboBox();
            this.filterlist = new System.Windows.Forms.ListBox();
            this.filter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(419, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Bug List";
            // 
            // listBoxbuglist
            // 
            this.listBoxbuglist.FormattingEnabled = true;
            this.listBoxbuglist.ItemHeight = 15;
            this.listBoxbuglist.Location = new System.Drawing.Point(61, 164);
            this.listBoxbuglist.Name = "listBoxbuglist";
            this.listBoxbuglist.Size = new System.Drawing.Size(128, 274);
            this.listBoxbuglist.TabIndex = 2;
            this.listBoxbuglist.SelectedIndexChanged += new System.EventHandler(this.listBoxbuglist_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(93, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Bugs to fix";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(594, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Code Type:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(610, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Severity:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(637, 335);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "OS:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(569, 391);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 15);
            this.label6.TabIndex = 7;
            this.label6.Text = "Date Submitted:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.Location = new System.Drawing.Point(343, 531);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 51);
            this.button1.TabIndex = 8;
            this.button1.Text = "Fix the Bug";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Fix_the_Bug);
            // 
            // userlabel
            // 
            this.userlabel.AutoSize = true;
            this.userlabel.Location = new System.Drawing.Point(25, 23);
            this.userlabel.Name = "userlabel";
            this.userlabel.Size = new System.Drawing.Size(38, 15);
            this.userlabel.TabIndex = 9;
            this.userlabel.Text = "label7";
            // 
            // bugnamebox
            // 
            this.bugnamebox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bugnamebox.Location = new System.Drawing.Point(343, 501);
            this.bugnamebox.Name = "bugnamebox";
            this.bugnamebox.ReadOnly = true;
            this.bugnamebox.Size = new System.Drawing.Size(116, 23);
            this.bugnamebox.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(366, 471);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 15);
            this.label7.TabIndex = 11;
            this.label7.Text = "Bug Name:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(618, 284);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 15);
            this.label8.TabIndex = 12;
            this.label8.Text = "Priority:";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox2.Location = new System.Drawing.Point(696, 175);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(145, 23);
            this.textBox2.TabIndex = 18;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox3.Location = new System.Drawing.Point(696, 225);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(145, 23);
            this.textBox3.TabIndex = 19;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox4.Location = new System.Drawing.Point(696, 280);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(145, 23);
            this.textBox4.TabIndex = 20;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox5.Location = new System.Drawing.Point(696, 335);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(145, 23);
            this.textBox5.TabIndex = 21;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox6.Location = new System.Drawing.Point(696, 388);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(145, 23);
            this.textBox6.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(598, 127);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 15);
            this.label14.TabIndex = 23;
            this.label14.Text = "Username:";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox7.Location = new System.Drawing.Point(696, 123);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(145, 23);
            this.textBox7.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(233, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 15);
            this.label9.TabIndex = 25;
            this.label9.Text = "Bugs assigned to me";
            // 
            // myassinedlist
            // 
            this.myassinedlist.FormattingEnabled = true;
            this.myassinedlist.ItemHeight = 15;
            this.myassinedlist.Location = new System.Drawing.Point(227, 164);
            this.myassinedlist.Name = "myassinedlist";
            this.myassinedlist.Size = new System.Drawing.Size(128, 274);
            this.myassinedlist.TabIndex = 26;
            this.myassinedlist.SelectedIndexChanged += new System.EventHandler(this.myassinedlist_SelectedIndexChanged);
            // 
            // checkhistory
            // 
            this.checkhistory.BackColor = System.Drawing.Color.DodgerBlue;
            this.checkhistory.Location = new System.Drawing.Point(743, 557);
            this.checkhistory.Name = "checkhistory";
            this.checkhistory.Size = new System.Drawing.Size(141, 51);
            this.checkhistory.TabIndex = 27;
            this.checkhistory.Text = "Check Bug History";
            this.checkhistory.UseVisualStyleBackColor = false;
            this.checkhistory.Click += new System.EventHandler(this.checkhistory_Click);
            // 
            // severitybox
            // 
            this.severitybox.FormattingEnabled = true;
            this.severitybox.Location = new System.Drawing.Point(399, 127);
            this.severitybox.Name = "severitybox";
            this.severitybox.Size = new System.Drawing.Size(128, 23);
            this.severitybox.TabIndex = 28;
            this.severitybox.SelectedIndexChanged += new System.EventHandler(this.severitybox_SelectedIndexChanged);
            // 
            // filterlist
            // 
            this.filterlist.FormattingEnabled = true;
            this.filterlist.ItemHeight = 15;
            this.filterlist.Location = new System.Drawing.Point(399, 164);
            this.filterlist.Name = "filterlist";
            this.filterlist.Size = new System.Drawing.Size(128, 274);
            this.filterlist.TabIndex = 30;
            this.filterlist.SelectedIndexChanged += new System.EventHandler(this.filterlist_SelectedIndexChanged);
            // 
            // filter
            // 
            this.filter.AutoSize = true;
            this.filter.Location = new System.Drawing.Point(430, 96);
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(62, 15);
            this.filter.TabIndex = 31;
            this.filter.Text = "Filter Bugs";
            // 
            // BugList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(898, 622);
            this.Controls.Add(this.filter);
            this.Controls.Add(this.filterlist);
            this.Controls.Add(this.severitybox);
            this.Controls.Add(this.checkhistory);
            this.Controls.Add(this.myassinedlist);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.bugnamebox);
            this.Controls.Add(this.userlabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBoxbuglist);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Name = "BugList";
            this.Text = "Bug List";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxbuglist;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label userlabel;
        private System.Windows.Forms.TextBox bugnamebox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox myassinedlist;
        private System.Windows.Forms.Button checkhistory;
        private System.Windows.Forms.ComboBox severitybox;
        private System.Windows.Forms.ListBox filterlist;
        private System.Windows.Forms.Label filter;
    }
}