﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.IO;
using ColorCode;

namespace BugTrackerApp
{
    public partial class BugHistory : Form
    {
        //for bug name and and user to be passed between forms
        public string AuthUser { get; set; }
        public string AuthBug { get; set; }

        public BugHistory(string user, string bug)
        {
            this.AuthUser = user;
            this.AuthBug = bug;
            InitializeComponent();
            populateListBox();
        }

        SqlCeConnection mySqlConnection;

        /// <summary>
        /// add values to the list box
        /// </summary>
        public void populateListBox()
        {
            //connected to database
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");
      
            String selcmd = "SELECT BugHistoryId FROM BugHistory WHERE bugname = '"+this.AuthBug+ "' ORDER BY BugHistoryId DESC";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);


            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                commitlist.Items.Clear();

                while (mySqlDataReader.Read())
                {                   
                    commitlist.Items.Add(mySqlDataReader["BugHistoryId"]);//displays commitlist
                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// when a value from the commit list is picked, items related to that value will populate textboxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commitlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selcmd2 = "SELECT * FROM BugHistory WHERE BugHistoryId = '"+commitlist.Text+"'";
            SqlCeCommand mySqlCommand2 = new SqlCeCommand(selcmd2, mySqlConnection);

            try
            {//values are set
                SqlCeDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    textBox4.Text = mySqlDataReader2["bugname"].ToString();
                    textBox9.Text = mySqlDataReader2["codetype"].ToString();
                    textBox7.Text = mySqlDataReader2["username"].ToString();
                    textBox5.Text = mySqlDataReader2["status"].ToString();
                    textBox6.Text = mySqlDataReader2["date"].ToString();
                    textBox8.Text = mySqlDataReader2["fixersname"].ToString();                 
                    textBox1.Text = mySqlDataReader2["initialcode"].ToString();
                    string initCode = mySqlDataReader2["initialcode"].ToString();
                    string colorizedinitCode = new CodeColorizer().Colorize(initCode, Languages.CSharp);
                    initcodetext.DocumentText = colorizedinitCode;
                    textBox2.Text = mySqlDataReader2["finalcode"].ToString();
                    string solCode = mySqlDataReader2["finalcode"].ToString();
                    string colorizedsolCode = new CodeColorizer().Colorize(solCode, Languages.CSharp);
                    solcodetext.DocumentText = colorizedsolCode;
                    textBox3.Text = mySqlDataReader2["description"].ToString();
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// save file dialog will pop up allowing the user to save code as a text file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";

            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string name = saveFileDialog1.FileName;
                File.WriteAllText(name, textBox2.Text);
            }

        }


        /// <summary>
        /// save file dialog will pop up allowing the user to save code as a text file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";//allows to be saved as a txt file

            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string name = saveFileDialog1.FileName;
                File.WriteAllText(name, textBox1.Text);
            }
        }
    }


}
