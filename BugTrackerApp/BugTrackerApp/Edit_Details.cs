﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugTrackerApp
{
    public partial class Edit_Details : Form
    {
        /// <summary>
        /// username that passes through classes
        /// </summary>
        public string AuthUser { get; set; }

        public Edit_Details(string user)
        {
            InitializeComponent();
            this.AuthUser = user;
            label6.Text = user;
            Form_load();
            textBox3.PasswordChar = '*';
            textBox4.PasswordChar = '*';
        }

        /// <summary>
        /// initiates the connection 
        /// </summary>
        SqlCeConnection mySqlConnection;

        /// <summary>
        /// connects to the database
        /// </summary>
        private void Form_load()
        {
            mySqlConnection =
                   new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");
            String selcmd = "SELECT * FROM members WHERE username = '"+this.AuthUser+"'";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                while (mySqlDataReader.Read())
                {
                    textBox1.Text = mySqlDataReader["firstname"].ToString();
                    textBox2.Text = mySqlDataReader["surname"].ToString();
                    textBox5.Text = mySqlDataReader["email"].ToString();
                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// check the validity of user input
        /// </summary>
        /// <returns></returns>
        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (
                 string.IsNullOrEmpty(textBox1.Text)
                || string.IsNullOrEmpty(textBox2.Text)
                || string.IsNullOrEmpty(textBox3.Text)
                || string.IsNullOrEmpty(textBox5.Text)
                || string.IsNullOrEmpty(textBox4.Text))
            {
                MessageBox.Show("Error: Please fill in all the fields");
                rtnvalue = false;
            }

            if (textBox3.Text != textBox4.Text)
            {
                MessageBox.Show("Error: Your Passwords do not match");
                rtnvalue = false;

            }

            return (rtnvalue);

        }

        /// <summary>
        /// update user data in the database
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="surname"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="commandString"></param>
        public void updaterecord(String firstname, String surname, String password, String email, String commandString)
        {
            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);
                cmdInsert.Parameters.AddWithValue("@firstname", firstname);
                cmdInsert.Parameters.AddWithValue("@surname", surname);
                cmdInsert.Parameters.AddWithValue("@password", password);
                cmdInsert.Parameters.AddWithValue("@email", email);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// updates record when the edit button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void edit_button(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                String commandString = "UPDATE members SET firstname = @firstname, surname = @surname, password = @password, email = @email WHERE username = '" + this.AuthUser + "'";
                updaterecord(textBox1.Text, textBox2.Text, textBox3.Text, textBox5.Text, commandString);
                MessageBox.Show("Your details are changed");
                this.Close();
            }
        }
    }
}
