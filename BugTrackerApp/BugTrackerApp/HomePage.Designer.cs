﻿namespace BugTrackerApp
{
    partial class HomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textUserName = new System.Windows.Forms.TextBox();
            this.textUserPassword = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.registerbutton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProjectToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.checkProjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportBugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fixBugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.queriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.myQueriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.myAnswersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userName = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.edit_user_button = new System.Windows.Forms.Button();
            this.logoutbutton = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(199, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Login";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Username";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Password";
            // 
            // textUserName
            // 
            this.textUserName.Location = new System.Drawing.Point(124, 75);
            this.textUserName.Name = "textUserName";
            this.textUserName.Size = new System.Drawing.Size(226, 23);
            this.textUserName.TabIndex = 5;
            // 
            // textUserPassword
            // 
            this.textUserPassword.Location = new System.Drawing.Point(124, 118);
            this.textUserPassword.Name = "textUserPassword";
            this.textUserPassword.Size = new System.Drawing.Size(226, 23);
            this.textUserPassword.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(173, 189);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 45);
            this.button1.TabIndex = 7;
            this.button1.Text = "Login";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Login_button);
            // 
            // registerbutton
            // 
            this.registerbutton.BackColor = System.Drawing.Color.DodgerBlue;
            this.registerbutton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.registerbutton.Location = new System.Drawing.Point(582, 44);
            this.registerbutton.Name = "registerbutton";
            this.registerbutton.Size = new System.Drawing.Size(118, 27);
            this.registerbutton.TabIndex = 8;
            this.registerbutton.Text = "Register";
            this.registerbutton.UseVisualStyleBackColor = false;
            this.registerbutton.Click += new System.EventHandler(this.register_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DodgerBlue;
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(444, 218);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(131, 46);
            this.button3.TabIndex = 9;
            this.button3.Text = "My Queries";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.User_Bugs);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DodgerBlue;
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Location = new System.Drawing.Point(300, 271);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(133, 46);
            this.button4.TabIndex = 10;
            this.button4.Text = "Fix a Bug";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.Fix_Bug_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DodgerBlue;
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(300, 218);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(133, 46);
            this.button5.TabIndex = 11;
            this.button5.Text = "Report a Bug";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.Report_Bug_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textUserName);
            this.panel1.Controls.Add(this.textUserPassword);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(142, 168);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(444, 282);
            this.panel1.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Symbol", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(272, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(202, 47);
            this.label4.TabIndex = 13;
            this.label4.Text = "BugTracker";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DodgerBlue;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.bugsToolStripMenuItem,
            this.queriesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(714, 24);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem1,
            this.checkProjectsToolStripMenuItem});
            this.newProjectToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.newProjectToolStripMenuItem.Text = "Projects";
            // 
            // newProjectToolStripMenuItem1
            // 
            this.newProjectToolStripMenuItem1.Name = "newProjectToolStripMenuItem1";
            this.newProjectToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.newProjectToolStripMenuItem1.Text = "New Project";
            this.newProjectToolStripMenuItem1.Click += new System.EventHandler(this.new_project_menu);
            // 
            // checkProjectsToolStripMenuItem
            // 
            this.checkProjectsToolStripMenuItem.Name = "checkProjectsToolStripMenuItem";
            this.checkProjectsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.checkProjectsToolStripMenuItem.Text = "Check Projects";
            this.checkProjectsToolStripMenuItem.Click += new System.EventHandler(this.check_project_menu);
            // 
            // bugsToolStripMenuItem
            // 
            this.bugsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportBugsToolStripMenuItem,
            this.fixBugsToolStripMenuItem});
            this.bugsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bugsToolStripMenuItem.Name = "bugsToolStripMenuItem";
            this.bugsToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.bugsToolStripMenuItem.Text = "Bugs";
            // 
            // reportBugsToolStripMenuItem
            // 
            this.reportBugsToolStripMenuItem.Name = "reportBugsToolStripMenuItem";
            this.reportBugsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.reportBugsToolStripMenuItem.Text = "Report Bugs";
            this.reportBugsToolStripMenuItem.Click += new System.EventHandler(this.report_bug_menu);
            // 
            // fixBugsToolStripMenuItem
            // 
            this.fixBugsToolStripMenuItem.Name = "fixBugsToolStripMenuItem";
            this.fixBugsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.fixBugsToolStripMenuItem.Text = "Fix Bugs";
            this.fixBugsToolStripMenuItem.Click += new System.EventHandler(this.fix_bug_menu);
            // 
            // queriesToolStripMenuItem
            // 
            this.queriesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.myQueriesToolStripMenuItem,
            this.myAnswersToolStripMenuItem});
            this.queriesToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.queriesToolStripMenuItem.Name = "queriesToolStripMenuItem";
            this.queriesToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.queriesToolStripMenuItem.Text = "Queries";
            // 
            // myQueriesToolStripMenuItem
            // 
            this.myQueriesToolStripMenuItem.Name = "myQueriesToolStripMenuItem";
            this.myQueriesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.myQueriesToolStripMenuItem.Text = "My Queries";
            this.myQueriesToolStripMenuItem.Click += new System.EventHandler(this.my_query_menu);
            // 
            // myAnswersToolStripMenuItem
            // 
            this.myAnswersToolStripMenuItem.Name = "myAnswersToolStripMenuItem";
            this.myAnswersToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.myAnswersToolStripMenuItem.Text = "My Answers";
            this.myAnswersToolStripMenuItem.Click += new System.EventHandler(this.my_answer_menu);
            // 
            // userName
            // 
            this.userName.AutoSize = true;
            this.userName.Location = new System.Drawing.Point(12, 44);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(73, 15);
            this.userName.TabIndex = 15;
            this.userName.Text = "Please Login";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DodgerBlue;
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Location = new System.Drawing.Point(161, 218);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(129, 46);
            this.button6.TabIndex = 16;
            this.button6.Text = "New Project";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.newProject);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.DodgerBlue;
            this.button7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Location = new System.Drawing.Point(444, 271);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(131, 46);
            this.button7.TabIndex = 17;
            this.button7.Text = "My Answers";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.my_answers);
            // 
            // edit_user_button
            // 
            this.edit_user_button.BackColor = System.Drawing.Color.DodgerBlue;
            this.edit_user_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.edit_user_button.Location = new System.Drawing.Point(582, 509);
            this.edit_user_button.Name = "edit_user_button";
            this.edit_user_button.Size = new System.Drawing.Size(118, 27);
            this.edit_user_button.TabIndex = 18;
            this.edit_user_button.Text = "Edit user details";
            this.edit_user_button.UseVisualStyleBackColor = false;
            this.edit_user_button.Click += new System.EventHandler(this.edit_user_details);
            // 
            // logoutbutton
            // 
            this.logoutbutton.BackColor = System.Drawing.Color.DodgerBlue;
            this.logoutbutton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.logoutbutton.Location = new System.Drawing.Point(582, 44);
            this.logoutbutton.Name = "logoutbutton";
            this.logoutbutton.Size = new System.Drawing.Size(118, 27);
            this.logoutbutton.TabIndex = 19;
            this.logoutbutton.Text = "Logout";
            this.logoutbutton.UseVisualStyleBackColor = false;
            this.logoutbutton.Click += new System.EventHandler(this.logoutbutton_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DodgerBlue;
            this.button9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button9.Location = new System.Drawing.Point(161, 271);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(129, 46);
            this.button9.TabIndex = 20;
            this.button9.Text = "Projects";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.MyProjects);
            // 
            // HomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(714, 549);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.logoutbutton);
            this.Controls.Add(this.edit_user_button);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.registerbutton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "HomePage";
            this.Text = "Bug Tracker";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textUserName;
        private System.Windows.Forms.TextBox textUserPassword;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button registerbutton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label userName;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button edit_user_button;
        private System.Windows.Forms.Button logoutbutton;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem checkProjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportBugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fixBugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem queriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem myQueriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem myAnswersToolStripMenuItem;
    }
}

