﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Security.Cryptography;

namespace BugTrackerApp
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
            connection();
            txtpassword.PasswordChar = '*';
            txtrepassword.PasswordChar = '*';
        }

        /// <summary>
        /// initiates the connection
        /// </summary>
        SqlCeConnection mySqlConnection;

        public void connection()
        {
            mySqlConnection =
                   new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");

            mySqlConnection.Open();
        }

        /// <summary>
        /// hashes the users password value for optimum security
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Md5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
     
        /// <summary>
        /// clears all the text boxes once the main the results are added to the database
        /// </summary>
        public void cleartxtBoxes()
        {
            txtusername.Text = txtfirstname.Text = txtsurname.Text = txtpassword.Text = txtrepassword.Text = txtEmail.Text = "";
        }

        /// <summary>
        /// validates user input
        /// </summary>
        /// <returns></returns>
        public bool checkInputs()
        {
            bool rtnvalue = true;
            String existingusercheck = "SELECT COUNT(*) FROM members WHERE username = '"+txtusername.Text+"'";
            SqlCeCommand cmdcheckuser = new SqlCeCommand(existingusercheck, mySqlConnection);
            int userCount = (int)cmdcheckuser.ExecuteScalar();

            if (userCount > 0)
            {
                MessageBox.Show("Error: User already exists");
                rtnvalue = false;

            }                   

            if (
                string.IsNullOrEmpty(txtusername.Text) 
                || string.IsNullOrEmpty(txtfirstname.Text) 
                || string.IsNullOrEmpty(txtsurname.Text) 
                || string.IsNullOrEmpty(txtpassword.Text)
                || string.IsNullOrEmpty(txtEmail.Text)
                || string.IsNullOrEmpty(txtrepassword.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            if (txtpassword.Text != txtrepassword.Text)
            {
                MessageBox.Show("Error: Your Passwords do not match");
                rtnvalue = false;

            }

            return (rtnvalue);

        }


        /// <summary>
        /// inserts users data into the members table in the database
        /// </summary>
        /// <param name="username"></param>
        /// <param name="firstname"></param>
        /// <param name="surname"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="commandString"></param>
        public void insertRecord(String username, String firstname, String surname, String password, String email, String commandString)
        {
           
            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                // cmdInsert.Parameters.AddWithValue("@ID", ID);
                cmdInsert.Parameters.AddWithValue("@username", username);
                cmdInsert.Parameters.AddWithValue("@firstname", firstname);
                cmdInsert.Parameters.AddWithValue("@surname", surname);
                cmdInsert.Parameters.AddWithValue("@password", password);
                cmdInsert.Parameters.AddWithValue("@email", email);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// when the submit button is clicked queries are carried out to add the results into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submit_button(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                string hashed_password = Md5Hash(txtpassword.Text);
                String commandString = "INSERT INTO members(username, firstname, surname, password, email) VALUES (@username, @firstname, @surname, @password, @email)";
                insertRecord(txtusername.Text, txtfirstname.Text, txtsurname.Text, hashed_password, txtEmail.Text, commandString);                
                cleartxtBoxes();
                MessageBox.Show("New user added to the System");
                this.Close();

            }
        }
    }
}
