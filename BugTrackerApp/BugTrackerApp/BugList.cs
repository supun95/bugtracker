﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugTrackerApp
{
    public partial class BugList : Form
    {
        //username passed through
        public string AuthUser { get; set; }
       

        public BugList(string user)
        {
            this.AuthUser = user;
            InitializeComponent();
            populateListBox();
            populateListBox2();
            userlabel.Text = this.AuthUser;
            combofill();

        }
        //initiate the connection
        SqlCeConnection mySqlConnection;
        public String chosenbug;

        /// <summary>
        /// values are added to list boxes
        /// </summary>
        public void populateListBox()
        {

            mySqlConnection =
                    new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");

            String selcmd = "SELECT * FROM Bugs where status != 'Bug is Fixed' AND status != 'Pending Approval'";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);


            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                listBoxbuglist.Items.Clear();//clear items to prevent repetitions

                while (mySqlDataReader.Read())
                {                   
                    listBoxbuglist.Items.Add(mySqlDataReader["bugname"]);
                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// populate items that are assigned to the particular user
        /// </summary>
        public void populateListBox2()
        {
            String selcmd2 = "SELECT * FROM Bugs where status != 'Bug is Fixed' AND status != 'Pending Approval' AND assignuser = '"+this.AuthUser+"'";
            SqlCeCommand mySqlCommand2 = new SqlCeCommand(selcmd2, mySqlConnection);


            try
            {
                SqlCeDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();

                myassinedlist.Items.Clear();

                while (mySqlDataReader2.Read())
                {
                    myassinedlist.Items.Add(mySqlDataReader2["bugname"]);
                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// combofill allows the user to pick bugs to fix in terms of its severity
        /// </summary>
        public void combofill()
        {
            severitybox.Items.Add("Critical");
            severitybox.Items.Add("Major");
            severitybox.Items.Add("Moderate");
            severitybox.Items.Add("Minor");
        }

        /// <summary>
        /// when list box item is clicked text boxes will be populated according to the query
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxbuglist_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selcmd2 = "SELECT * FROM Bugs WHERE bugname = '"+listBoxbuglist.Text+"'";
            SqlCeCommand mySqlCommand2 = new SqlCeCommand(selcmd2, mySqlConnection);

            try
            {
                SqlCeDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    bugnamebox.Text = mySqlDataReader2["bugname"].ToString();                  
                    textBox2.Text = mySqlDataReader2["code"].ToString();                   
                    textBox3.Text = mySqlDataReader2["severity"].ToString();                   
                    textBox4.Text = mySqlDataReader2["priority"].ToString();                   
                    textBox5.Text = mySqlDataReader2["os"].ToString();                   
                    textBox6.Text = mySqlDataReader2["datesubmitted"].ToString();
                    textBox7.Text = mySqlDataReader2["username"].ToString();
                    chosenbug = mySqlDataReader2["bugname"].ToString(); 
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// when list box item is clicked text boxes will be populated according to the query
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void myassinedlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selcmd3 = "SELECT * FROM Bugs WHERE bugname = '" + myassinedlist.Text + "'";
            SqlCeCommand mySqlCommand3 = new SqlCeCommand(selcmd3, mySqlConnection);
            try
            {               
                SqlCeDataReader mySqlDataReader3 = mySqlCommand3.ExecuteReader();
                while (mySqlDataReader3.Read())
                {
                    bugnamebox.Text = mySqlDataReader3["bugname"].ToString();
                    textBox2.Text = mySqlDataReader3["code"].ToString();
                    textBox3.Text = mySqlDataReader3["severity"].ToString();
                    textBox4.Text = mySqlDataReader3["priority"].ToString();
                    textBox5.Text = mySqlDataReader3["os"].ToString();
                    textBox6.Text = mySqlDataReader3["datesubmitted"].ToString();
                    textBox7.Text = mySqlDataReader3["username"].ToString();
                    chosenbug = mySqlDataReader3["bugname"].ToString();
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// when list box item is clicked text boxes will be populated according to the query
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void severitybox_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selcmd4 = "SELECT * FROM Bugs WHERE severity = '" + severitybox.Text + "' AND status != 'Bug is Fixed' AND status != 'Pending Approval'";
            SqlCeCommand mySqlCommand4 = new SqlCeCommand(selcmd4, mySqlConnection);
            filterlist.Items.Clear();

            try
            {
                SqlCeDataReader mySqlDataReader4 = mySqlCommand4.ExecuteReader();
                while (mySqlDataReader4.Read())
                {
                    filterlist.Items.Add(mySqlDataReader4["bugname"]);
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// when list box item is clicked text boxes will be populated according to the query
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void filterlist_SelectedIndexChanged(object sender, EventArgs e)
        {

            String selcmd5 = "SELECT * FROM Bugs WHERE bugname = '" + filterlist.Text + "'";
            SqlCeCommand mySqlCommand5 = new SqlCeCommand(selcmd5, mySqlConnection);
            

            try
            {
                //textBox1.Text = "";
                SqlCeDataReader mySqlDataReader5 = mySqlCommand5.ExecuteReader();
                while (mySqlDataReader5.Read())
                {
                    bugnamebox.Text = mySqlDataReader5["bugname"].ToString();
                    textBox2.Text = mySqlDataReader5["code"].ToString();
                    textBox3.Text = mySqlDataReader5["severity"].ToString();
                    textBox4.Text = mySqlDataReader5["priority"].ToString();
                    textBox5.Text = mySqlDataReader5["os"].ToString();
                    textBox6.Text = mySqlDataReader5["datesubmitted"].ToString();
                    textBox7.Text = mySqlDataReader5["username"].ToString();
                    chosenbug = mySqlDataReader5["bugname"].ToString();
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// checks user input: whether value is null or not
        /// </summary>
        /// <returns></returns>
        public bool checkInputs()
        {
            bool rtnvalue = true;                   

            if (
                string.IsNullOrEmpty(bugnamebox.Text))
            {
                MessageBox.Show("Error: Please select a bug from the listbox");
                rtnvalue = false;
            }

            return (rtnvalue);

        }
        /// <summary>
        /// open new forms according to button clicks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fix_the_Bug(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                FixBugForm newForm = new FixBugForm(chosenbug, this.AuthUser);
                newForm.Show();
                this.Close();              
            }
            
        }

        /// <summary>
        /// open new forms according to button clicks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkhistory_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(bugnamebox.Text))
            {
                BugHistory newForm = new BugHistory(this.AuthUser, chosenbug);
                newForm.Show();
            }
            else
            {
                MessageBox.Show("Please select a bug to check Bug History");
            }
        }

        
    }
}
