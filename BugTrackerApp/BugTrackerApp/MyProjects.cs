﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugTrackerApp
{
    public partial class MyProjects : Form
    {
        //users details being passed through
        public string AuthUser { get; set; }

        public MyProjects(string user)
        {
            InitializeComponent();
            this.AuthUser = user;
            label1.Text = user;
            populateListBox();
        }

        /// <summary>
        /// initiates the conncetion
        /// </summary>
        SqlCeConnection mySqlConnection;

        /// <summary>
        /// Shows a list of projects in the database in a listbox
        /// </summary>
        public void populateListBox()
        {

            mySqlConnection =
                    new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");

            String selcmd = "SELECT * FROM Projects";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                projectlistbox.Items.Clear();

                while (mySqlDataReader.Read())
                {                   
                    projectlistbox.Items.Add(mySqlDataReader["projectname"]);
                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// when the project is clicked, projects details are displayed seperately
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void projectlistbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            String selcmd2 = "SELECT * FROM Projects WHERE projectname = '" + projectlistbox.Text + "'";
            SqlCeCommand mySqlCommand2 = new SqlCeCommand(selcmd2, mySqlConnection);

            try
            {
                SqlCeDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();
                while (mySqlDataReader2.Read())
                {
                    textBox1.Text = mySqlDataReader2["projectname"].ToString();
                    textBox2.Text = mySqlDataReader2["date"].ToString();
                    textBox3.Text = mySqlDataReader2["projectdesc"].ToString();
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            String selcmd3 = "SELECT * FROM Bugs WHERE projectname = '" + projectlistbox.Text + "'";
            SqlCeCommand mySqlCommand3 = new SqlCeCommand(selcmd3, mySqlConnection);


            
            buglistbox.Items.Clear();

            try
            {
                SqlCeDataReader mySqlDataReader3 = mySqlCommand3.ExecuteReader();
                while (mySqlDataReader3.Read())
                {
                    buglistbox.Items.Add(mySqlDataReader3["bugname"]);
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        /// <summary>
        /// validates user inputs
        /// </summary>
        /// <returns></returns>
        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (
                 string.IsNullOrEmpty(textBox4.Text))
            {
                MessageBox.Show("Error: Please choose a bug");
                rtnvalue = false;
            }        

            return (rtnvalue);
        }

        /// <summary>
        /// transfers the value that the user picked into a textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buglistbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox4.Text = buglistbox.Text;
        }

        /// <summary>
        /// opens the BugHistory form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bug_activity(object sender, EventArgs e)
        {
            if (checkInputs())           
            {
                BugHistory newForm = new BugHistory(this.AuthUser, textBox4.Text);
                newForm.Show();
            }
            
        }

       
    }
}
