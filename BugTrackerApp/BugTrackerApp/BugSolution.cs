﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Net.Mail;
using ColorCode;

namespace BugTrackerApp
{
    public partial class BugSolution : Form
    {
        //values that are passed through between forms
        public string AuthUser { get; set; }
        public string AuthBug { get; set; }

        public BugSolution(string bug, string user)
        {
            this.AuthBug = bug;
            this.AuthUser = user;
            InitializeComponent();
            label20.Text = user;
            Form_load();
            statusbox();
           

        }

        //initiate connection
        SqlCeConnection mySqlConnection;

        /// <summary>
        /// connects to the database
        /// </summary>
        private void Form_load()
        {
            mySqlConnection =
                   new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");
            String selcmd = "SELECT * FROM Bugs WHERE bugname = '" + this.AuthBug + "'";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                //populate textboxes accordingly
                while (mySqlDataReader.Read())
                {
                    
                    
                    textBox5.Text = mySqlDataReader["bugname"].ToString();                                        
                    textBox9.Text = mySqlDataReader["code"].ToString();                  
                    textBox7.Text = mySqlDataReader["fixersname"].ToString();                   
                    textBox6.Text = mySqlDataReader["DL"].ToString();                   
                    textBox8.Text = mySqlDataReader["platform"].ToString();                   
                    textBox10.Text = mySqlDataReader["os"].ToString();                   
                    textBox11.Text = mySqlDataReader["priority"].ToString();
                    textBox3.Text = mySqlDataReader["problemcode"].ToString();
                    string myCode = mySqlDataReader["problemcode"].ToString();
                    string colorizedmyCode = new CodeColorizer().Colorize(myCode, Languages.CSharp);
                    mycode.DocumentText = colorizedmyCode;
                    textBox1.Text = mySqlDataReader["modifiedcode"].ToString();
                    string modCode = mySqlDataReader["modifiedcode"].ToString();
                    string colorizedinitCode = new CodeColorizer().Colorize(modCode, Languages.CSharp);
                    modcode.DocumentText = colorizedinitCode;
                    textBox2.Text = mySqlDataReader["modifieddesc"].ToString();
                    textBox4.Text = mySqlDataReader["projectname"].ToString();

                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// allows the user to check bug status
        /// </summary>
        public void statusbox()
        {
            status.Items.Add("Bug is Fixed");
            status.Items.Add("Bug is Not Fixed");
        }

        /// <summary>
        /// checks validity of user input
        /// </summary>
        /// <returns></returns>
        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (
                string.IsNullOrEmpty(status.Text))
            {
                MessageBox.Show("Error: Please specify the bug status");
                rtnvalue = false;
            }

            if (status.Text != "Bug is Fixed" && status.Text != "Bug is Not Fixed")
            {
                MessageBox.Show("Error: Please select the status from the given list");
                rtnvalue = false;
            }

            if (status.Text == "Bug is Not Fixed" && string.IsNullOrEmpty(textBox12.Text))
            {
                MessageBox.Show("Error: Please provide a reason why the big is not fixed");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        /// <summary>
        /// updates the users information into the database
        /// </summary>
        /// <param name="status"></param>
        /// <param name="bugname"></param>
        /// <param name="commandString"></param>
        public void updaterecord(String status, String bugname, String commandString)
        {
            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);
                cmdInsert.Parameters.AddWithValue("@status", status);
                cmdInsert.Parameters.AddWithValue("@bugname", bugname);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Inserts record history into the database
        /// </summary>
        /// <param name="bugname"></param>
        /// <param name="description"></param>
        /// <param name="date"></param>
        /// <param name="username"></param>
        /// <param name="fixersname"></param>
        /// <param name="status"></param>
        /// <param name="initialcode"></param>
        /// <param name="finalcode"></param>
        /// <param name="codetype"></param>
        /// <param name="commandString2"></param>
        public void insertRecordHistory(String bugname, String description, DateTime date, String username, String fixersname, String status, String initialcode, String finalcode, String codetype, String commandString2)
        {

            try
            {
                SqlCeCommand cmdInsert2 = new SqlCeCommand(commandString2, mySqlConnection);

                cmdInsert2.Parameters.AddWithValue("@bugname", bugname);
                cmdInsert2.Parameters.AddWithValue("@description", description);
                cmdInsert2.Parameters.AddWithValue("@date", date);
                cmdInsert2.Parameters.AddWithValue("@username", username);
                cmdInsert2.Parameters.AddWithValue("@fixersname", fixersname);
                cmdInsert2.Parameters.AddWithValue("@status", status);
                cmdInsert2.Parameters.AddWithValue("@initialcode", initialcode);
                cmdInsert2.Parameters.AddWithValue("@finalcode", finalcode);
                cmdInsert2.Parameters.AddWithValue("@codetype", codetype);
                cmdInsert2.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// open new forms according to button clicks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void History_Click(object sender, EventArgs e)
        {
            BugHistory newForm = new BugHistory(this.AuthUser, this.AuthBug);
            newForm.Show();
        }

        
        /// <summary>
        /// submits users values into the database after all the checks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submit_button(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                
                 String commandString = "UPDATE Bugs SET status = @status WHERE bugname = @bugname";
                 updaterecord(status.Text, this.AuthBug, commandString);
                 MessageBox.Show("Your bug status has changed");                                        

                if (status.Text == "Bug is Fixed")
                {
                    String commandString2 = "INSERT INTO BugHistory(bugname, description, date, username, fixersname, status, initialcode, finalcode, codetype) VALUES (@bugname, @description, @date, @username, @fixersname, @status, @initialcode, @finalcode, @codetype)";
                    insertRecordHistory(this.AuthBug, textBox2.Text, DateTime.Now, this.AuthUser, textBox7.Text, "Bug is Fixed", textBox3.Text, textBox1.Text, textBox9.Text, commandString2);
                }
                if (status.Text == "Bug is Not Fixed")
                {                                  
                    String commandString2 = "INSERT INTO BugHistory(bugname, description, date, username, fixersname, status, initialcode, finalcode, codetype) VALUES (@bugname, @description, @date, @username, @fixersname, @status, @initialcode, @finalcode, @codetype)";
                    insertRecordHistory(this.AuthBug, textBox12.Text, DateTime.Now, this.AuthUser, textBox7.Text, "Bug is Not Fixed", textBox3.Text, textBox1.Text, textBox9.Text, commandString2);                 
                }
                this.Close();
                    
            }
        }
    }
}
