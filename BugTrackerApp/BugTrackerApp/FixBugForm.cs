﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Web;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using ColorCode;

namespace BugTrackerApp
{
    public partial class FixBugForm : Form
    {
        //details that are passed between forms
        public string AuthBug { get; set; }
        public string AuthUser { get; set; }
        public String UserEmail;

        public FixBugForm(string bug, string user)
        {
            this.AuthBug = bug;
            this.AuthUser = user;
            InitializeComponent();
            Form_load();
        }

        /// <summary>
        /// initiates the connection
        /// </summary>
        SqlCeConnection mySqlConnection;

        /// <summary>
        /// connects to the database
        /// </summary>
        private void Form_load()
        {
            mySqlConnection =
                   new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");
            String selcmd = "SELECT * FROM Bugs WHERE bugname = '"+this.AuthBug+ "'";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                while (mySqlDataReader.Read())//populates listboxes accordingly
                {                   
                    textBox5.Text = mySqlDataReader["bugname"].ToString();                  
                    textBox6.Text = mySqlDataReader["username"].ToString();                   
                    textBox7.Text = mySqlDataReader["code"].ToString();
                    string sourceCode = mySqlDataReader["problemcode"].ToString();
                    string colorizedSourceCode = new CodeColorizer().Colorize(sourceCode, Languages.CSharp);
                    problemcode.DocumentText = colorizedSourceCode;
                    textBox8.Text = mySqlDataReader["platform"].ToString();                  
                    textBox9.Text = mySqlDataReader["os"].ToString();                  
                    textBox12.Text = mySqlDataReader["priority"].ToString();                    
                    textBox10.Text = mySqlDataReader["DL"].ToString();
                    textBox11.Text = mySqlDataReader["datesubmitted"].ToString();
                    textBox1.Text = mySqlDataReader["bugdescription"].ToString();
                    textBox2.Text = mySqlDataReader["problemcode"].ToString();

                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        
        /// <summary>
        /// checks the validity of user inputs
        /// </summary>
        /// <returns></returns>
        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (
                string.IsNullOrEmpty(textBox4.Text)
                || string.IsNullOrEmpty(textBox3.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        /// <summary>
        /// this method updates values in the database
        /// </summary>
        /// <param name="modifiedcode"></param>
        /// <param name="modifieddesc"></param>
        /// <param name="status"></param>
        /// <param name="datefixed"></param>
        /// <param name="fixersname"></param>
        /// <param name="bugname"></param>
        /// <param name="commandString"></param>
        public void updaterecord(String modifiedcode, String modifieddesc, String status, DateTime datefixed, String fixersname, String bugname, String commandString)
        {
            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@modifiedcode", modifiedcode);
                cmdInsert.Parameters.AddWithValue("@modifieddesc", modifieddesc);
                cmdInsert.Parameters.AddWithValue("@status", status);
                cmdInsert.Parameters.AddWithValue("@datefixed", datefixed);
                cmdInsert.Parameters.AddWithValue("@fixersname", fixersname);
                cmdInsert.Parameters.AddWithValue("@bugname", bugname);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// this method inserts values into the database
        /// </summary>
        /// <param name="bugname"></param>
        /// <param name="description"></param>
        /// <param name="date"></param>
        /// <param name="username"></param>
        /// <param name="fixersname"></param>
        /// <param name="status"></param>
        /// <param name="initialcode"></param>
        /// <param name="finalcode"></param>
        /// <param name="codetype"></param>
        /// <param name="commandString2"></param>
        public void insertRecordHistory(String bugname, String description, DateTime date, String username, String fixersname,String status, String initialcode, String finalcode, String codetype, String commandString2)
        {

            try
            {
                SqlCeCommand cmdInsert2 = new SqlCeCommand(commandString2, mySqlConnection);

                cmdInsert2.Parameters.AddWithValue("@bugname", bugname);
                cmdInsert2.Parameters.AddWithValue("@description", description);
                cmdInsert2.Parameters.AddWithValue("@date", date);
                cmdInsert2.Parameters.AddWithValue("@username", username);
                cmdInsert2.Parameters.AddWithValue("@fixersname", fixersname);
                cmdInsert2.Parameters.AddWithValue("@status", status);
                cmdInsert2.Parameters.AddWithValue("@initialcode", initialcode);
                cmdInsert2.Parameters.AddWithValue("@finalcode", finalcode);
                cmdInsert2.Parameters.AddWithValue("@codetype", codetype);
                cmdInsert2.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// Alllows the user to update and insert code into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BugFixed_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                String commandString = "UPDATE Bugs SET modifiedcode = @modifiedcode, modifieddesc = @modifieddesc, status = @status, datefixed = @datefixed, fixersname = @fixersname WHERE bugname = @bugname";
                updaterecord(textBox4.Text, textBox3.Text, "Pending Approval", DateTime.Now, this.AuthUser, this.AuthBug, commandString);
                
                String commandString2 = "INSERT INTO BugHistory(bugname, description, date, username, fixersname, status, initialcode, finalcode, codetype) VALUES (@bugname, @description, @date, @username, @fixersname, @status, @initialcode, @finalcode, @codetype)";
                insertRecordHistory(this.AuthBug, textBox3.Text, DateTime.Now, textBox6.Text, this.AuthUser, "Pending Approval", textBox2.Text, textBox4.Text, textBox7.Text, commandString2);
                

                MessageBox.Show("Your bug has been submitted");

                if (emailcheckBox.Checked)//check this
                {
                    String selctemail = "SELECT * FROM members WHERE username = '" + textBox6.Text + "'";
                    SqlCeCommand mySqlCommand = new SqlCeCommand(selctemail, mySqlConnection);
                    try
                    {
                        SqlCeDataReader mySqlDataReader5 = mySqlCommand.ExecuteReader();
                        while (mySqlDataReader5.Read())
                        {
                            UserEmail = mySqlDataReader5["email"].ToString();
                        }
                    }

                    catch (SqlCeException ex)
                    {

                        MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    ///sends an email to the user showing notifications and solutions
                    SmtpClient client = new SmtpClient();
                    client.Port = 587;
                    client.Host = "smtp.gmail.com";
                    client.EnableSsl = true;
                    client.Timeout = 10000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential("stackquestionsme@gmail.com", "110195sd");

                    MailMessage mm = new MailMessage("stackquestionsme@gmail.com", UserEmail, "test", "Your bug '"+ this.AuthBug+ "' was fixed by user: " + this.AuthUser + Environment.NewLine + "Log into BugTracker to accept the answer" + Environment.NewLine + Environment.NewLine + "Reply Him: " + UserEmail);
                    mm.BodyEncoding = UTF8Encoding.UTF8;
                    mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                    client.Send(mm);
                    MessageBox.Show("Email Sent");
                    this.Close();
                }
            }
           
        }

        /// <summary>
        /// allows the user to download problem code in a form of a text file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void downloadcode_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";

            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string name = saveFileDialog1.FileName;
                File.WriteAllText(name, textBox2.Text);
            }
        }


        /// <summary>
        /// allows the user to upload code into the system
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uploadcode_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox4.Text = System.IO.File.ReadAllText(openFileDialog1.FileName);
                string solutioncodetext = textBox4.Text;
                solutioncodetext = System.IO.File.ReadAllText(openFileDialog1.FileName);
                string colorizedSourceCode = new CodeColorizer().Colorize(solutioncodetext, Languages.CSharp);
                solutioncode.DocumentText = colorizedSourceCode;
            }
        }      
    }
}
