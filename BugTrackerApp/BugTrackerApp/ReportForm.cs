﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Net.Mail;

namespace BugTrackerApp
{
    public partial class ReportForm : Form
    {
        /// <summary>
        /// This retireves the user data form form1 and allows that data to be passed between different forms.
        /// </summary>
        public string AuthUser { get ; set; }
        public String EmailAddress;
        public String ReplyEmailAddress;

        
        public ReportForm(string user)
        {
            this.AuthUser = user;
            InitializeComponent();
            connection();
            userlabel.Text = this.AuthUser;
            combofill();
            projectfill();
            asignuserfill();

        }


        //public String username;
        public int DLValue;
        public string myvalue;


        /// <summary>
        /// initiates the connection
        /// </summary>
        SqlCeConnection mySqlConnection;
        
        public void connection()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=H:\Leeds Beckett Computing\Advanced Software Engineering A\BugTrackerApp\BugTrackerApp\mydatabase.sdf");

            mySqlConnection.Open();         
        }

        /// <summary>
        /// manually adds data into comboboxes
        /// </summary>
        public void combofill()
        {
            severity.Items.Add("Critical");
            severity.Items.Add("Major");
            severity.Items.Add("Moderate");
            severity.Items.Add("Minor");

            platform.Items.Add("All");
            platform.Items.Add("PC");
            platform.Items.Add("Mobile");
            platform.Items.Add("Other");

            OS.Items.Add("Microsoft");
            OS.Items.Add("UNIX");
            OS.Items.Add("Apple OS");
            OS.Items.Add("Android");
            OS.Items.Add("Other");
            OS.Items.Add("All");

            priority.Items.Add("P1");
            priority.Items.Add("P2");
            priority.Items.Add("P3");
            priority.Items.Add("P4");
            priority.Items.Add("P5");

            code.Items.Add("C#");
            code.Items.Add("C++");
            code.Items.Add("C");
            code.Items.Add("Java");
            code.Items.Add("Android");
            code.Items.Add("PHP");
            code.Items.Add("HTML");
            code.Items.Add("CSS");
            code.Items.Add("Perl");
            code.Items.Add("JavaScript");
            code.Items.Add("Python");
            code.Items.Add("Ruby");
            code.Items.Add("VB");
            code.Items.Add("Oracle");
            code.Items.Add("MySQL");
            code.Items.Add("Other");

        }

        /// <summary>
        /// displays existing projects from the database 
        /// </summary>
        public void projectfill()
        {
            String selcmd2 = "SELECT * FROM Projects";

            SqlCeCommand mySqlCommand2 = new SqlCeCommand(selcmd2, mySqlConnection);

            try
            {
                SqlCeDataReader mySqlDataReader2 = mySqlCommand2.ExecuteReader();              

                while (mySqlDataReader2.Read())
                {

                    projectbox.Items.Add(mySqlDataReader2["projectname"]);


                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// allows to assign the problem to an existing user
        /// </summary>
        public void asignuserfill()
        {
            String selcmd3 = "SELECT * FROM members";

            SqlCeCommand mySqlCommand3 = new SqlCeCommand(selcmd3, mySqlConnection);

            try
            {
                SqlCeDataReader mySqlDataReader3 = mySqlCommand3.ExecuteReader();

                while (mySqlDataReader3.Read())
                {

                    assignbox.Items.Add(mySqlDataReader3["username"]);


                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>
        /// clear values in the textboxes once the form is submitted
        /// </summary>
        public void cleartxtBoxes()
        {
            bugname.Text = severity.Text = platform.Text = OS.Text = priority.Text = bugdescription.Text = problemcode.Text = code.Text = projectbox.Text =  assignbox.Text= "";
        }

        /// <summary>
        /// obtains the trackbar value for difficulty level
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DLScroll(object sender, EventArgs e)
        {
            //obtain the trackbar value for difficulty level
            DLValue = this.trackBar1.Value;
        }

        /// <summary>
        /// validates user input
        /// </summary>
        /// <returns></returns>
        public bool checkInputs()
        {
            bool rtnvalue = true;
            String existingbugcheck = "SELECT COUNT(*) FROM Bugs WHERE bugname = '"+bugname.Text+"'";
            SqlCeCommand cmdcheckuser = new SqlCeCommand(existingbugcheck, mySqlConnection);
            int bugCount = (int)cmdcheckuser.ExecuteScalar();

            if (bugCount > 0)
            {
                MessageBox.Show("Error: Bug name already exists. Provide an alternative name");
                rtnvalue = false;

            }

            if (
                string.IsNullOrEmpty(bugname.Text)
                || string.IsNullOrEmpty(severity.Text)
                || string.IsNullOrEmpty(platform.Text)
                || string.IsNullOrEmpty(OS.Text)
                || string.IsNullOrEmpty(priority.Text)
                || string.IsNullOrEmpty(bugdescription.Text)
                || string.IsNullOrEmpty(code.Text)
                || string.IsNullOrEmpty(assignbox.Text)
                || string.IsNullOrEmpty(projectbox.Text)
                || string.IsNullOrEmpty(problemcode.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }         

            return (rtnvalue);

        }

        /// <summary>
        /// Inserts a New Record to the Database using all attributes provided by the user
        /// </summary>
        /// <param name="bugname"></param>
        /// <param name="severity"></param>
        /// <param name="platform"></param>
        /// <param name="OS"></param>
        /// <param name="priority"></param>
        /// <param name="bugdescription"></param>
        /// <param name="problemcode"></param>
        /// <param name="DL"></param>
        /// <param name="username"></param>
        /// <param name="status"></param>
        /// <param name="datesubmitted"></param>
        /// <param name="code"></param>
        /// <param name="projectname"></param>
        /// <param name="assignuser"></param>
        /// <param name="commandString"></param>
        public void insertRecord(String bugname, String severity, String platform, String OS, String priority, String bugdescription, String problemcode, int DL, String username, String status, DateTime datesubmitted, String code, String projectname, String assignuser, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@bugname", bugname);
                cmdInsert.Parameters.AddWithValue("@severity", severity);
                cmdInsert.Parameters.AddWithValue("@platform", platform);
                cmdInsert.Parameters.AddWithValue("@OS", OS);
                cmdInsert.Parameters.AddWithValue("@priority", priority);
                cmdInsert.Parameters.AddWithValue("@bugdescription", bugdescription);
                cmdInsert.Parameters.AddWithValue("@problemcode", problemcode);
                cmdInsert.Parameters.AddWithValue("@DL", DL);
                cmdInsert.Parameters.AddWithValue("@username", username);
                cmdInsert.Parameters.AddWithValue("@status", status);
                cmdInsert.Parameters.AddWithValue("@datesubmitted", datesubmitted);
                cmdInsert.Parameters.AddWithValue("@code", code);
                cmdInsert.Parameters.AddWithValue("@projectname", projectname); 
                cmdInsert.Parameters.AddWithValue("@assignuser", assignuser);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// Inserts a fraction of bug attributes to the bughistory table as the bugs very first commit
        /// </summary>
        /// <param name="bugname"></param>
        /// <param name="description"></param>
        /// <param name="date"></param>
        /// <param name="username"></param>
        /// <param name="status"></param>
        /// <param name="initialcode"></param>
        /// <param name="finalcode"></param>
        /// <param name="codetype"></param>
        /// <param name="commandString2"></param>
        public void insertRecordHistory(String bugname, String description, DateTime date, String username, String status, String initialcode, String finalcode, String codetype, String commandString2)
        {

            try
            {
                SqlCeCommand cmdInsert2 = new SqlCeCommand(commandString2, mySqlConnection);

                cmdInsert2.Parameters.AddWithValue("@bugname", bugname);
                cmdInsert2.Parameters.AddWithValue("@description", description);
                cmdInsert2.Parameters.AddWithValue("@date", date);
                cmdInsert2.Parameters.AddWithValue("@username", username);
                cmdInsert2.Parameters.AddWithValue("@status", status);
                cmdInsert2.Parameters.AddWithValue("@initialcode", initialcode);
                cmdInsert2.Parameters.AddWithValue("@finalcode", finalcode);
                cmdInsert2.Parameters.AddWithValue("@codetype", codetype);
                cmdInsert2.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// when the user clicks submit, results will be added to both bugs and bughistory tables
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Report_Submit(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                String commandString = "INSERT INTO Bugs(bugname, severity, platform, os, priority, bugdescription, problemcode, DL, username, status, datesubmitted, code, projectname, assignuser) VALUES (@bugname, @severity, @platform, @OS, @priority, @bugdescription, @problemcode, @DL, @username, @status, @datesubmitted, @code, @projectname, @assignuser)";
                insertRecord(bugname.Text, severity.Text, platform.Text, OS.Text, priority.Text, bugdescription.Text, problemcode.Text, DLValue, this.AuthUser, "Initial Question", DateTime.Now, code.Text, projectbox.Text, assignbox.Text, commandString);

                String commandString2 = "INSERT INTO BugHistory(bugname, description, date, username, status, initialcode, finalcode, codetype) VALUES (@bugname, @description, @date, @username, @status, @initialcode, @finalcode, @codetype)";
                insertRecordHistory(bugname.Text, bugdescription.Text, DateTime.Now, this.AuthUser, "Initial Question", problemcode.Text, "The problem is not yet resolved by any user", code.Text, commandString2);


                String selctemail = "SELECT * FROM members WHERE username = '" + assignbox.Text + "'";
                SqlCeCommand mySqlCommand = new SqlCeCommand(selctemail, mySqlConnection);              
                try
                {
                    SqlCeDataReader mySqlDataReader5 = mySqlCommand.ExecuteReader();
                    while (mySqlDataReader5.Read())
                    {
                        EmailAddress = mySqlDataReader5["email"].ToString();
                    }
                }

                catch (SqlCeException ex)
                {

                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                String selctemail2 = "SELECT * FROM members WHERE username = '" + this.AuthUser + "'";
                SqlCeCommand mySqlCommand2 = new SqlCeCommand(selctemail2, mySqlConnection);
                try
                {
                    SqlCeDataReader mySqlDataReader6 = mySqlCommand2.ExecuteReader();
                    while (mySqlDataReader6.Read())
                    {
                        ReplyEmailAddress = mySqlDataReader6["email"].ToString();
                    }
                }

                catch (SqlCeException ex)
                {

                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //An email will be sent to the assigned user showing all the attributes of the bug
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("stackquestionsme@gmail.com", "110195sd");

                //email message
                MailMessage mm = new MailMessage("stackquestionsme@gmail.com", EmailAddress, "Bug Report from BugTracker", "Queried By: " + this.AuthUser + Environment.NewLine + "Bug Name: " +bugname.Text+ Environment.NewLine + "Code Type: "+code.Text + Environment.NewLine +"Platform: "+ platform.Text+Environment.NewLine + "Project: " + projectbox.Text + Environment.NewLine + "Severity: " + severity.Text + Environment.NewLine + "Operating System: " + OS.Text + Environment.NewLine + "Difficulty Level: " + DLValue + Environment.NewLine + "Priority: " + priority.Text + Environment.NewLine + Environment.NewLine + "Log in to BugTracker application and click Bugs -> Fix Bugs to obtain the general bug list and the bugs that are assigned to yourself. Select the appropriate bug and click fix bug" + Environment.NewLine + Environment.NewLine + "Original Poster's contact email: " + ReplyEmailAddress);
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                client.Send(mm);

                MessageBox.Show("Your bug has been submitted and an Email is sent to the assigned user");
                cleartxtBoxes();
                this.Close();

            }
 
        }

        /// <summary>
        /// Uploads a code from the users file list.It is much convinient than copying and pasting code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uploadbutton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                problemcode.Text = System.IO.File.ReadAllText(openFileDialog1.FileName);
            }
        }

    
    }
}
