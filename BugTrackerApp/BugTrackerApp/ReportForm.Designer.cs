﻿namespace BugTrackerApp
{
    partial class ReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bugname = new System.Windows.Forms.TextBox();
            this.bugdescription = new System.Windows.Forms.TextBox();
            this.problemcode = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.severity = new System.Windows.Forms.ComboBox();
            this.platform = new System.Windows.Forms.ComboBox();
            this.OS = new System.Windows.Forms.ComboBox();
            this.priority = new System.Windows.Forms.ComboBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.userlabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.code = new System.Windows.Forms.ComboBox();
            this.projectbox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.uploadbutton = new System.Windows.Forms.Button();
            this.assignbox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bug Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 519);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bug Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(138, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Code";
            // 
            // bugname
            // 
            this.bugname.Location = new System.Drawing.Point(141, 76);
            this.bugname.Name = "bugname";
            this.bugname.Size = new System.Drawing.Size(140, 23);
            this.bugname.TabIndex = 3;
            // 
            // bugdescription
            // 
            this.bugdescription.Location = new System.Drawing.Point(199, 516);
            this.bugdescription.Multiline = true;
            this.bugdescription.Name = "bugdescription";
            this.bugdescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.bugdescription.Size = new System.Drawing.Size(710, 52);
            this.bugdescription.TabIndex = 4;
            this.bugdescription.WordWrap = false;
            // 
            // problemcode
            // 
            this.problemcode.Location = new System.Drawing.Point(199, 226);
            this.problemcode.Multiline = true;
            this.problemcode.Name = "problemcode";
            this.problemcode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.problemcode.Size = new System.Drawing.Size(710, 249);
            this.problemcode.TabIndex = 5;
            this.problemcode.WordWrap = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.Location = new System.Drawing.Point(517, 588);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 27);
            this.button1.TabIndex = 6;
            this.button1.Text = "Submit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Report_Submit);
            // 
            // severity
            // 
            this.severity.FormattingEnabled = true;
            this.severity.Location = new System.Drawing.Point(141, 122);
            this.severity.Name = "severity";
            this.severity.Size = new System.Drawing.Size(140, 23);
            this.severity.TabIndex = 7;
            // 
            // platform
            // 
            this.platform.FormattingEnabled = true;
            this.platform.Location = new System.Drawing.Point(642, 75);
            this.platform.Name = "platform";
            this.platform.Size = new System.Drawing.Size(140, 23);
            this.platform.TabIndex = 8;
            // 
            // OS
            // 
            this.OS.FormattingEnabled = true;
            this.OS.Location = new System.Drawing.Point(381, 119);
            this.OS.Name = "OS";
            this.OS.Size = new System.Drawing.Size(140, 23);
            this.OS.TabIndex = 9;
            // 
            // priority
            // 
            this.priority.FormattingEnabled = true;
            this.priority.Location = new System.Drawing.Point(642, 119);
            this.priority.Name = "priority";
            this.priority.Size = new System.Drawing.Size(140, 23);
            this.priority.TabIndex = 10;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(322, 167);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(493, 45);
            this.trackBar1.TabIndex = 11;
            this.trackBar1.Scroll += new System.EventHandler(this.DLScroll);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(61, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "Severity";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(582, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 15);
            this.label5.TabIndex = 13;
            this.label5.Text = "Platform";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(590, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 15);
            this.label6.TabIndex = 14;
            this.label6.Text = "Priority";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(349, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 15);
            this.label7.TabIndex = 15;
            this.label7.Text = "OS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(226, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "Difficulty Level";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(495, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 25);
            this.label9.TabIndex = 17;
            this.label9.Text = "Report Bugs";
            // 
            // userlabel
            // 
            this.userlabel.AutoSize = true;
            this.userlabel.Location = new System.Drawing.Point(64, 23);
            this.userlabel.Name = "userlabel";
            this.userlabel.Size = new System.Drawing.Size(29, 15);
            this.userlabel.TabIndex = 18;
            this.userlabel.Text = "user";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(337, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 15);
            this.label10.TabIndex = 20;
            this.label10.Text = "Code";
            // 
            // code
            // 
            this.code.FormattingEnabled = true;
            this.code.Location = new System.Drawing.Point(381, 75);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(140, 23);
            this.code.TabIndex = 21;
            // 
            // projectbox
            // 
            this.projectbox.FormattingEnabled = true;
            this.projectbox.Location = new System.Drawing.Point(890, 74);
            this.projectbox.Name = "projectbox";
            this.projectbox.Size = new System.Drawing.Size(140, 23);
            this.projectbox.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(836, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 15);
            this.label11.TabIndex = 23;
            this.label11.Text = "Project";
            // 
            // uploadbutton
            // 
            this.uploadbutton.BackColor = System.Drawing.Color.DodgerBlue;
            this.uploadbutton.Location = new System.Drawing.Point(199, 482);
            this.uploadbutton.Name = "uploadbutton";
            this.uploadbutton.Size = new System.Drawing.Size(141, 27);
            this.uploadbutton.TabIndex = 24;
            this.uploadbutton.Text = "Upload Your Code";
            this.uploadbutton.UseVisualStyleBackColor = false;
            this.uploadbutton.Click += new System.EventHandler(this.uploadbutton_Click);
            // 
            // assignbox
            // 
            this.assignbox.FormattingEnabled = true;
            this.assignbox.Location = new System.Drawing.Point(890, 119);
            this.assignbox.Name = "assignbox";
            this.assignbox.Size = new System.Drawing.Size(140, 23);
            this.assignbox.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(810, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 15);
            this.label12.TabIndex = 26;
            this.label12.Text = "Assign To";
            // 
            // ReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(1102, 622);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.assignbox);
            this.Controls.Add(this.uploadbutton);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.projectbox);
            this.Controls.Add(this.code);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.userlabel);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.priority);
            this.Controls.Add(this.OS);
            this.Controls.Add(this.platform);
            this.Controls.Add(this.severity);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.problemcode);
            this.Controls.Add(this.bugdescription);
            this.Controls.Add(this.bugname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Name = "ReportForm";
            this.Text = "Report Bug";
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox bugname;
        private System.Windows.Forms.TextBox bugdescription;
        private System.Windows.Forms.TextBox problemcode;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox severity;
        private System.Windows.Forms.ComboBox platform;
        private System.Windows.Forms.ComboBox OS;
        private System.Windows.Forms.ComboBox priority;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label userlabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox code;
        private System.Windows.Forms.ComboBox projectbox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button uploadbutton;
        private System.Windows.Forms.ComboBox assignbox;
        private System.Windows.Forms.Label label12;
    }
}